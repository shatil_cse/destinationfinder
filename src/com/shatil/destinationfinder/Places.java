package com.shatil.destinationfinder;

import static com.shatil.destinationfinder.MainActivity.bmp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

public class Places {
	ProgressDialog dialog;
	int countresult = 0;
	String imageurl[],place_id[],place_name[],vicinity[],rating[],address[],phonenumber[];
	LatLng place_latlng[];
	Bitmap bitmaps[];
	int alreadyexist[];
	int settedindex = 0;
	ImageView iv;
	TextView tvplace,tvvicinity,tvaddress,tvphone,tvrating;
	Details_download_listner ddl;
	public Places(String type, LatLng src,
			Details_download_listner ddl) {
		this.ddl = ddl;
		new readFromGooglePlaceAPI()
				.execute("https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
						+ "location="
						+ src.latitude
						+ ","
						+ src.longitude
						+ "&radius=10000&sensor=true&"
						+ "key=AIzaSyCnVMKF4bDTJgv8HkSGOuIpDNQ07YyJ8RQ&types="
						+ type);
	}
	
	public class readFromGooglePlaceAPI extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... param) {
			return readJSON(param[0]);
		}

		protected void onPostExecute(String str) {
			try {
				JSONObject root = new JSONObject(str);
				JSONArray results = root.getJSONArray("results");
				countresult = results.length();
				initializevar();
				
				for (int i = 0; i < countresult; i++) {
					JSONObject arrayItems = results.getJSONObject(i);
					JSONObject geometry = arrayItems.getJSONObject("geometry");
					JSONObject location = geometry.getJSONObject("location");
					try {
						JSONArray photos = arrayItems.getJSONArray("photos");
						imageurl[i] = "https://maps.googleapis.com/maps/api/place/photo?maxwidth="
								+ 400
								+ "&photoreference="
								+ photos.getJSONObject(0).getString(
										"photo_reference")
								+ "&key=AIzaSyCnVMKF4bDTJgv8HkSGOuIpDNQ07YyJ8RQ";
					} catch (Exception e) {
						// Toast.makeText(context, e.toString(),
						// Toast.LENGTH_LONG).show();
						e.printStackTrace();
					}
					try {
						vicinity[i]="Vicinity : No Vicinity Found";
						rating[i] = "Rating : No Rating Found";
						place_name[i]="No Place Name Found";
						Double lat = Double.parseDouble(location
								.getString("lat"));
						Double lng = Double.parseDouble(location
								.getString("lng"));
						place_id[i] = arrayItems.getString("place_id");
						place_latlng[i] = new LatLng(lat, lng);
						place_name[i]=arrayItems.getString("name")+"  ";
						vicinity[i] = "Vicinity : "+arrayItems.getString("vicinity");
						rating[i]="Rating : "+arrayItems.getString("rating");
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				// Toast.makeText(context, results.toString(),
				// Toast.LENGTH_LONG).show();

			} catch (Exception e) {
				
			}
			ddl.on_search_coplete("search_complete");
		}
	}
	private void initializevar() {
		// TODO Auto-generated method stub
		imageurl= new String[countresult];		
		place_id = new String[countresult];
		place_name = new String[countresult];
		vicinity=new String[countresult];
		rating=new String[countresult];
		place_latlng= new LatLng[countresult];
		address=new String[countresult];
		phonenumber=new String[countresult];
		bitmaps=new Bitmap[countresult];
		alreadyexist=new int[countresult];
		for(int i=0;i<countresult;i++) alreadyexist[i]=0;
	}
	
	public String readJSON(String URL) {
		StringBuilder sb = new StringBuilder();
		HttpGet httpGet = new HttpGet(URL);
		HttpClient client = new DefaultHttpClient();

		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
			} else {
				Log.e("JSON", "Couldn't find JSON file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public String[] getlist() {
		String tempst[] = new String[countresult];
		try {
			for (int i = 0; i < countresult; i++)
				tempst[i] = place_name[i]+"  "+vicinity[i]+" "+rating[i];
		} catch (Exception e) {
				}
		return tempst;
	}
	
	public int getmarkerindex(String str)
	{
		int index=0;
		for(int i=0;i<countresult;i++)
		{
			if((place_name[i]+vicinity[i]+"\n"+rating[i]+"more..").equals(str))
			{
				index=i;
				break;
			}
		}
		return index;
	}
	public long getresult_len()
	{
		return countresult;
	}
	public String getplace_id(int index)
	{
		return place_id[index];
	}
	public LatLng getplace_latlng(int index)
	{
		return place_latlng[index];
	}
	public String getvicinity(int index)
	{
		return vicinity[index];
	}
	public String getplaces_name(int index)
	{
		return place_name[index];
	}
	public String getrating(int index)
	{
		return rating[index];
	}
	public String getaddress(int index)
	{
		return address[index];
	}
	public String getphone(int index)
	{
		return phonenumber[index];
	}
	public LatLng getlatlng(int index)
	{
		return place_latlng[index];
	}
	public Bitmap getimage(int index)
	{
		return bitmaps[index];
	}
	public void loaddetails(int index,Details_download_listner ddl) {
		settedindex=index;
		this.ddl=ddl;
		if(alreadyexist[settedindex]==0)
		{
			new readPlaceDetails().execute("https://maps.googleapis.com/maps/api/place/details/json?placeid="+place_id[settedindex]+"&key=AIzaSyCnVMKF4bDTJgv8HkSGOuIpDNQ07YyJ8RQ");
		}
		else
		{
			ddl.on_details_download_complete("already_exists");
			ddl.on_image_download_complete("already_exists");
		}
	}
	public class readPlaceDetails extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... param) {
			return readJSON(param[0]);
		}

		protected void onPostExecute(String str) {
			try {
				JSONObject root = new JSONObject(str);
				JSONObject results = root.getJSONObject("result");
				try{
					if(imageurl[settedindex]==null) {
						JSONArray photos = results.getJSONArray("photos");
						imageurl[settedindex] = "https://maps.googleapis.com/maps/api/place/photo?maxwidth="
								+ 400
								+ "&photoreference="
								+ photos.getJSONObject(0).getString(
										"photo_reference")
								+ "&key=AIzaSyCnVMKF4bDTJgv8HkSGOuIpDNQ07YyJ8RQ";
					}
					if(imageurl[settedindex]!=null) 
					{
						new readImage().execute(imageurl[settedindex]);
					}
					else
					{
						bitmaps[settedindex]=bmp;
						ddl.on_image_download_complete("no_photo_exists");
					}
				} catch (Exception e) {
					bitmaps[settedindex]=bmp;
					e.printStackTrace();
				}
				try {
					address[settedindex]="Address : No Address Found..";
					phonenumber[settedindex]="Phone Number : No Phone Number Found..";
					address[settedindex]="Address : "+results.getString("formatted_address");
					phonenumber[settedindex]="Phone Number : "+results.getString("international_phone_number");
				} catch (Exception e) {
					e.printStackTrace();
				}
				ddl.on_details_download_complete("download_complete");
			}catch(Exception e)
			{
				ddl.on_details_download_complete("Error:"+e.toString());
			}
		}
	}

	public class readImage extends AsyncTask<String, Void, Bitmap> {
		@Override
		protected Bitmap doInBackground(String... param) {
			try{
				return readimage(param[0]);
			}catch(Exception e)
			{
				return null;
			}
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			if (result == null) result=bmp;
			bitmaps[settedindex]=result;
			ddl.on_image_download_complete("download_complete");
			super.onPostExecute(result);
		}
	}

	private Bitmap readimage(String URL) {
		// TODO Auto-generated method stub
		Bitmap bimage = null;
		HttpGet httpGet = new HttpGet(URL);
		HttpClient client = new DefaultHttpClient();
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() > 0) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				bimage = BitmapFactory.decodeStream(content);
			} else {
				Log.e("JSON",
						"-------------------Couldn't find JSON file-----------------------------------");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bimage;
	}
}

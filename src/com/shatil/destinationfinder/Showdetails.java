package com.shatil.destinationfinder;

import static com.shatil.destinationfinder.MainActivity.searchnearby;

import com.shatil.destinationfinder.R;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
public class Showdetails extends Activity {
	ImageView back;
	ImageView iv;
	TextView tvplace, tvvicinity, tvaddress, tvphone, tvrating;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.showdetails);
		int index=getIntent().getIntExtra("index",0);
		back = (ImageView) findViewById(R.id.slidedown);
		tvplace = (TextView) findViewById(R.id.place_name);
		tvvicinity = (TextView) findViewById(R.id.vicinity);
		tvaddress = (TextView) findViewById(R.id.address);
		tvphone = (TextView) findViewById(R.id.phone);
		tvrating = (TextView) findViewById(R.id.rating);
		iv = (ImageView) findViewById(R.id.placeimage);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		showdetails(index);
	}
	public void showdetails(final int index)
	{
		iv.setImageResource(R.drawable.loading);
		tvplace.setText("Name Loading..");
		tvvicinity.setText("Vicinity Loading..");
		tvaddress.setText("Address Loading..");
		tvphone.setText("Phone Number Loading..");
		tvrating.setText("Rating Loading..");
		Details_download_listner ddl=new Details_download_listner() {
			
			@Override
			public void on_image_download_complete(String tag) {
				// TODO Auto-generated method stub
				try{
					iv.setImageBitmap(searchnearby.getimage(index));
				}catch(Exception e)
				{
					iv.setImageBitmap(MainActivity.bmp);
				}
			}
			
			@Override
			public void on_details_download_complete(String tag) {
				// TODO Auto-generated method stub
				tvplace.setText(searchnearby.getplaces_name(index));
				tvvicinity.setText(searchnearby.getvicinity(index));
				tvaddress.setText(searchnearby.getaddress(index));
				tvphone.setText(searchnearby.getphone(index));
				tvrating.setText(searchnearby.getrating(index));
			}

			@Override
			public void on_search_coplete(String tag) {
				// TODO Auto-generated method stub
				
			}
		};
		searchnearby.loaddetails(index,ddl);
	}
}

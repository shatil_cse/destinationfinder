package com.shatil.destinationfinder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.shatil.destinationfinder.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements LocationListener,
		ConnectionCallbacks, OnConnectionFailedListener,OnItemSelectedListener{
	Savescrienshoot savescrien;
	int setmarker = 0;
	GoogleMap map;
	static Bitmap bmp;
	LatLng dest, src;
	int status=0;
	private Location mylocation;
	public static Places searchnearby; //here static .....................................................
	private LocationClient mLocationClient;
	String placestosearch[] = { "Select option","school","hospital", "police","local_government_office","post_office","library","fire_station","train_station","atm", "restaurant", "stadium",
			"bank", "gas_station", "zoo","gym","food","cafe","pharmacy","insurance_agency","mosque","place_of_worship","university"};
	String selectedplace = "restaurant";
	private static final LocationRequest REQUEST = LocationRequest.create()
			.setInterval(100000).setFastestInterval(10000)
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	int status_curr=1;
	Spinner ptype, presult;
	ProgressDialog dialog;
	TextView btnDraw;
	TextView savesnap;
	Details_download_listner ddl=new Details_download_listner() {
		
		@Override
		public void on_search_coplete(String tag) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			String place_det[] = searchnearby.getlist();
			setresultspinner(place_det);
			long res_len=searchnearby.getresult_len();
			for(int i=0;i<res_len;i++)
			{
				try{
					addmarker(searchnearby.getlatlng(i), searchnearby.getplaces_name(i),
							searchnearby.getvicinity(i)+"\n"+searchnearby.getrating(i)+"more..", i);
				}catch(Exception e)
				{
					
				}
			}
		}
		
		@Override
		public void on_image_download_complete(String tag) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void on_details_download_complete(String tag) {
			// TODO Auto-generated method stub
			
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		dialog = ProgressDialog.show(this, "",
				"Please wait", true);
		try{
			savescrien = new Savescrienshoot();
			bmp=BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.nophoto);
			// Getting reference to SupportMapFragment of the activity_main
			SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map);
			// Getting reference to Button
			btnDraw = (TextView) findViewById(R.id.btn_draw);
			savesnap = (TextView) findViewById(R.id.savesnapshoot);
			// Getting Map for the SupportMapFragment
			map = fm.getMap();
			// Enable MyLocation Button in the Map
			map.setMyLocationEnabled(true);
			presult = (Spinner) findViewById(R.id.presult);
			ptype = (Spinner) findViewById(R.id.ptype);
			setsearchspinner();
		}catch(Exception e)
		{
			if(dialog.isShowing()) dialog.dismiss();
		}
		map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker arg0) {
				// TODO Auto-generated method stub
				String indexstring=arg0.getTitle()+arg0.getSnippet();
				Intent i=new Intent(getApplicationContext(),Showdetails.class);
				i.putExtra("index", searchnearby.getmarkerindex(indexstring));
				startActivity(i);
			}
		});
		
		// Setting onclick event listener for the map
		map.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng point) {
				// Creating MarkerOptions
				if (setmarker > 0) {
					dest = point;
					MarkerOptions options = new MarkerOptions();
					// Setting the position of the marker
					options.position(point);
					options.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
					// Add new marker to the Google Map Android API V2
					map.addMarker(options);
					Toast.makeText(getApplicationContext(),
							"Marker set complete", Toast.LENGTH_LONG).show();
					setmarker = 0;
					btnDraw.setText("SET MARKER");
				}

			}
		});
		map.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker arg0) {
				// TODO Auto-generated method stub
				dest = arg0.getPosition();
				try {
					mylocation = getmylocation();
					src = new LatLng(mylocation.getLatitude(), mylocation
							.getLongitude());
					if (src != null && dest != null)
						new Makerutes(getApplicationContext(), map, src, dest);
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(),
							"Location error:" + e.toString(), Toast.LENGTH_LONG)
							.show();
				}
				return false;
			}
		});

		// Click event handler for Button btn_draw
		btnDraw.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					if (setmarker == 0) {
						setmarker = 1;
						btnDraw.setText("SELECT POSITION");
						Toast.makeText(getApplicationContext(),
								"Click Position To Set Marker",
								Toast.LENGTH_LONG).show();
					} else {
						setmarker = 0;
						btnDraw.setText("SET MARKER");
					}
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(),
							"Marker Not Set" + e.toString(), Toast.LENGTH_LONG)
							.show();

				}

			}
		});
		savesnap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					View mView = findViewById(R.id.map);
					Toast.makeText(
							getApplicationContext(), savescrien
											.captureMapScreen(
													getApplicationContext(),
													mView, map),
							Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(),
							"Snapshoot Error:" + e.toString(),
							Toast.LENGTH_LONG).show();
				}

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		try{
		setUpLocationClientIfNeeded();
		mLocationClient.connect();
		}catch(Exception e)
		{
			if(dialog.isShowing()) dialog.dismiss();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mLocationClient != null) {
			mLocationClient.disconnect();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		startActivity(new Intent(getApplicationContext(),ApplicationInformation.class));
		return super.onOptionsItemSelected(item);
	}
	private void setUpLocationClientIfNeeded() {
		try{
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(getApplicationContext(), this, // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}
		}catch(Exception e)
		{
			if(dialog.isShowing()) dialog.dismiss();
		}
	}
	@Override
	public void onLocationChanged(Location location) {
		 //Toast.makeText(getApplicationContext(), "Loc:"+location.getLatitude()+" "+location.getLongitude(),Toast.LENGTH_LONG).show();
		mylocation = location;
		try {
			if(dialog.isShowing()) dialog.dismiss();
			//Toast.makeText(getApplicationContext(), "Location changed", Toast.LENGTH_LONG).show();
			src = new LatLng(mylocation.getLatitude(),
						mylocation.getLongitude());
			if(status==0) 
			{
				setcameratolocation(mylocation.getLatitude(),
						mylocation.getLongitude());
				status++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			if(dialog.isShowing()) dialog.dismiss();
		}
	}
	@Override
	public void onConnected(Bundle connectionHint) {
		try{
		mLocationClient.requestLocationUpdates(REQUEST, this); // LocationListener
		//Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
		//if(dialog.isShowing()) dialog.dismiss();
		//Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
		}catch(Exception e)
		{
			if(dialog.isShowing()) dialog.dismiss();
		}
	}
	@Override
	public void onDisconnected() {
		// Do nothing
		Toast.makeText(getApplicationContext(), "Connection lost", Toast.LENGTH_LONG).show();
		if(dialog.isShowing()) dialog.dismiss();
		
	}
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// Do nothing
		if(dialog.isShowing()) dialog.dismiss();
		Toast.makeText(getApplicationContext(), "Connection failed", Toast.LENGTH_LONG).show();
	}

	public Location getmylocation() {
		return mylocation;
	}

	public void setcameratolocation(double latitude, double longitude) {
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(latitude, longitude)).zoom(11f).build();
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
			switch(arg0.getId())
			{
				case R.id.ptype:
					if(arg2>0)
					{
						dialog.show();
						map.clear();
						selectedplace = placestosearch[arg2];
						searchnearbyplaces();
					}
					break;
				case R.id.presult:
					dest=searchnearby.getlatlng(arg2);
					try {
						mylocation = getmylocation();
						src = new LatLng(mylocation.getLatitude(), mylocation
								.getLongitude());
						if (src != null && dest != null)
							new Makerutes(getApplicationContext(), map, src, dest);
					} catch (Exception e) {
						Toast.makeText(getApplicationContext(),
								"Location error:" + e.toString(), Toast.LENGTH_LONG)
								.show();
					}
					presult.setSelection(arg2, true);
					break;
			}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	public void searchnearbyplaces() {
		try {
			setcameratolocation(mylocation.getLatitude(),
					mylocation.getLongitude());
			src = new LatLng(mylocation.getLatitude(),
					mylocation.getLongitude());
			searchnearby = new Places(
					selectedplace, src, ddl);
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Sorry your Location not found", Toast.LENGTH_LONG).show();
			if(dialog.isShowing()) dialog.dismiss();
		}
	}
	private void setresultspinner(String[] place_det) {
		// TODO Auto-generated method stub
		ArrayAdapter<String> ap = new ArrayAdapter<String>(MainActivity.this,
				R.layout.textview, place_det);
		presult.setAdapter(ap);
		presult.setOnItemSelectedListener(this);
	}

	private void setsearchspinner() {
		// TODO Auto-generated method stub
		ArrayAdapter<String> ap = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_list_item_1, placestosearch);
		ptype.setAdapter(ap);
		ptype.setOnItemSelectedListener(this);
	}

	public void addmarker(LatLng point, String name, String snp, int tempcount) {
		MarkerOptions options = new MarkerOptions();
		// Setting the position of the marker
		options.position(point);
		options.icon(BitmapDescriptorFactory
				.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
		options.title(name).snippet(snp);
		// Add new marker to the Google Map Android API V2
		map.addMarker(options);
	}
}
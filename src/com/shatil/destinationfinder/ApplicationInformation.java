package com.shatil.destinationfinder;

import com.shatil.destinationfinder.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ApplicationInformation extends Activity{

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.applicationinformation);
		TextView tv1=(TextView)findViewById(R.id.infotitle);
		tv1.setTextSize(25);
		tv1.setText("Hi");
		tv1.setPadding(0, 10, 0, 10);
		tv1.setTextColor(Color.GREEN);
		TextView tv2=(TextView)findViewById(R.id.infobody);
		tv2.setText("Project Manager & Team Leader: Shahanur Zaman\nSystem tester:Md. Masud Rana\nDevelopers:Mosarrof hosain,Shathi akter,Rehana khatun\nComputer Science And Engineering.\nUniversity of Rajshahi,Bangladesh");
		tv2.setPadding(5, 5, 5, 5);
		tv2.setTextColor(Color.BLACK);
		TextView tv3=(TextView)findViewById(R.id.mailme);
		tv3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mailme();
			}
		});
		tv3.setText("Send an email to project manager");
		tv3.setTextColor(Color.GREEN);
		tv3.setPadding(5, 5, 5,5);
	}

	public void mailme()
	{
		String emailadd[]={"shatil200@gmail.com"};
		Intent emailintent=new Intent(android.content.Intent.ACTION_SEND);
		emailintent.putExtra(android.content.Intent.EXTRA_EMAIL,emailadd);//email addresses must be string array.....  
		emailintent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
		emailintent.putExtra(android.content.Intent.EXTRA_TEXT, "");
		emailintent.setType("plain/text");
		startActivity(emailintent);
		
	}
}
